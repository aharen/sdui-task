<?php

namespace Tests\Feature;

use Illuminate\Console\Command;
use Tests\Helpers\NewsHelpers;
use Tests\TestCase;

class DeleteOldNewsItemsCommandTest extends TestCase
{
    use NewsHelpers;

    /**
     * Test command: news:delete-old
     *
     */
    public function test_delete_old_news_items_command(): void
    {
        $news = $this->create_news(20);

        $this->assertDatabaseCount('news', 20);
        
        $update_news = $news->first();
        $update_news->created_at = now()->subDays(14);
        $update_news->save();
        
        $this->artisan('news:delete-old')
            ->assertExitCode(Command::SUCCESS);

        $this->assertDatabaseCount('news', 19);
    }
}
