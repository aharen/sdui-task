<?php

namespace Tests\Feature;

use App\Events\NewsCreatedEvent;
use Illuminate\Support\Facades\Event;
use Tests\Helpers\NewsHelpers;
use Tests\TestCase;

class NewsTest extends TestCase
{
    use NewsHelpers;

    /**
     * Test migrations and factories
     */
    public function test_factories_and_migrations(): void
    {
        $this->create_news(20);

        $this->assertDatabaseCount('users', 1);
        $this->assertDatabaseCount('news', 20);
    }

    /**
     * Test requires authentication
     */
    public function test_requires_to_be_authenticated(): void
    {
        $this->create_news(10);

        $response = $this->get('/news');

        $response->assertRedirect(route('login'));
        $response->assertStatus(302);
    }

    /**
     * Test display all articles
     * NewsController@index
     */
    public function test_can_view_all_news_articles(): void
    {
        $user = $this->create_user();
        $news = $this->create_news(20, $user);

        $response = $this->actingAs($user)
            ->get('/news');
            
        $response->assertOk();

        $response->assertJsonCount(15, 'data');
            
        $this->assertEquals(
            $news->take(15)->toArray(),
            $response->json('data')
        );
    }

    /**
     * Test create news
     * NewsController@store
     */
    public function test_can_store_news_article(): void
    {
        Event::fake([
            NewsCreatedEvent::class
        ]);

        $user = $this->create_user();

        $news_post = [
            'title' => 'This is a great breaking news',
            'content' => [
                'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rerum, voluptatem aliquid libero laboriosam maiores dicta maxime illum placeat! Amet aliquam cupiditate officia totam, recusandae unde odio nam voluptates nihil quas.',
                'Harum tempora deleniti labore cumque sed consectetur expedita ratione facilis iste. Aspernatur esse veniam ipsa repellendus voluptates, nesciunt quae non omnis accusantium doloremque eligendi obcaecati, cupiditate dolorum voluptatibus odit rem?',
            ],
            'user_id' => $user->id,
        ];

        $response = $this->actingAs($user)
            ->post('/news', $news_post);

        $response->assertOk();

        $response->assertJson($news_post);

        Event::assertDispatched(
            NewsCreatedEvent::class
        );
    }

    /**
     * Test can view single news
     * NewsController@show
     */
    public function test_can_view_single_news_item(): void
    {
        $user = $this->create_user();
        $news = $this->create_news(1, $user);

        $response = $this->actingAs($user)
            ->get("/news/{$news->id}");

        $response->assertOk();

        $response->assertJson($news->toArray());
    }

    /**
     * Test can patch news item
     * NewsController@patch
     */
    public function test_can_patch_news_item(): void
    {
        $user = $this->create_user();
        $news = $this->create_news(1, $user);

        $updated_content = [
            'title' => 'New title',
        ];

        $response = $this->actingAs($user)
            ->patch("/news/{$news->id}", $updated_content);

        $response->assertOk();

        $response->assertJson(array_merge(
            $news->toArray(),
            $updated_content
        ));
    }

    /**
     * Test can delete news item
     * NewsController@destroy
     */
    public function test_can_delete_news_item(): void
    {
        $user = $this->create_user();
        $news = $this->create_news(1, $user);
        
        $this->assertDatabaseCount('news', 1);

        $response = $this->actingAs($user)
            ->delete("/news/{$news->id}");

        $response->assertOk();

        $this->assertDatabaseCount('news', 0);

        $response->assertSeeText('Item was deleted');
    }
}
