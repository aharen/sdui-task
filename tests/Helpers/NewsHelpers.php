<?php

namespace Tests\Helpers;

use App\Models\News;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;

trait NewsHelpers
{
    use DatabaseMigrations;
    use RefreshDatabase;

    /**
     * Helper function: creates a user
     */
    private function create_user(): User
    {
        return User::factory()
            ->create();
    }

    /**
     * Helper function: creates a news item
     */
    public function create_news(int $count = 1, User $user = null): Collection|News
    {
        return News::factory()
            ->when($count > 1, fn ($factory) => $factory->count($count))
            ->for($user ?? $this->create_user())
            ->create();
    }
}
