<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class NewsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $content = collect($this->faker->paragraphs(rand(2, 10)))
            ->map(fn ($paragraph) => "<p>{$paragraph}</p>")
            ->toArray();

        return [
            'title' => $this->faker->sentence(),
            'content' => $content,
            'user_id' => User::factory(),
        ];
    }
}
