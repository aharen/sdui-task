<?php

namespace App\Console\Commands;

use App\Models\News;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class DeleteOldNewsItemsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'news:delete-old';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deletes news items that are older than 14 days.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $news_expired = News::query()
            ->where('created_at', '<=', now()->subDays(14))
            ->delete();

        Log::info("news:delete-old - deleted {$news_expired} news items");

        return Command::SUCCESS;
    }
}
