<?php

namespace App\Events;

use App\Models\News;
use Illuminate\Support\Facades\Log;

class NewsCreatedEvent
{
    public function __construct(News $news)
    {
        Log::info('NewsCreatedEvent: News item was created');
        Log::info($news);
    }
}
