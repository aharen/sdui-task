## Sdui Task

System requirements and setup instructions.

**Requirements:**

- PHP 8.0+
- Composer 2+

### Installation

clone the repository

```
git clone https://bitbucket.org/aharen/sdui-task.git
```

go in to project dir and install composer dependencies

```
cd sdui-task && composer install
```

create `.env` file from `.env.example` and generate application key

```
cp .env.example .env && php artisan key:generate
```

create database and run migrations

```
touch database/storage/database.sqlite && php artisan migrate
```

run test

```
./vendor/bin/phpunit 
```

### Notes:

On the below linked branch, please find a refactor that is a personal preference. It is refactored to use invokable controllers. Personally, I find that this cleans up the code a lot and helps in maintaining huge projects :)

- [https://bitbucket.org/aharen/sdui-task/src/invokable-controller/](https://bitbucket.org/aharen/sdui-task/src/invokable-controller/)