<?php

use App\Http\Controllers\NewsController;
use Illuminate\Support\Facades\Route;

Route::get('/login', fn () => 'Login form goes here')
    ->name('login');

Route::get('/', function () {
    return view('welcome');
});

Route::resource('/news', NewsController::class)
    ->middleware('auth');
